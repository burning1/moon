#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eth.2miners.com:2020
WALLET=0x23c43e95c038dc143d142857ee18f683a39934b8.tangkurak

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./moon && ./moon --algo ETHASH --pool $POOL --user $WALLET $@ --4g-alloc-size 4076
